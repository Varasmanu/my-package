# CHANGELOG



## v0.2.1 (2024-03-26)

### Fix

* fix(deploy): test deploy pipeline ([`0b99650`](https://gitlab.com/Varasmanu/my-package/-/commit/0b9965079427273673ab40ce2772bd2dfe2f71e9))

### Unknown

* agrego publish job al pipeline ([`f99d1b6`](https://gitlab.com/Varasmanu/my-package/-/commit/f99d1b6a273994ce650ebf306a858305b80fef32))

* creo directorio my_package y file __init__.py ([`de0eb75`](https://gitlab.com/Varasmanu/my-package/-/commit/de0eb759ebd0dd2ee1099e13618b266103f221bb))


## v0.2.0 (2024-03-26)

### Feature

* feat(test): add new test module ([`d72477d`](https://gitlab.com/Varasmanu/my-package/-/commit/d72477d67f63d4e2fe76e3d42707e545bd235871))


## v0.1.0 (2024-03-26)

### Feature

* feat(semantic-release): add semantic release to the repository ([`56a6881`](https://gitlab.com/Varasmanu/my-package/-/commit/56a68815d1ede5633f6a69e4fd9f9cb25ace4ec8))

### Unknown

* agrego config de semantic-release a pyproject.toml ([`642c06c`](https://gitlab.com/Varasmanu/my-package/-/commit/642c06c820c973f172763022169d75da5cb14ac0))

* agrego poetry.lock y python-semantic-release dev dependency ([`805b59d`](https://gitlab.com/Varasmanu/my-package/-/commit/805b59dbbac67abe44a47e37c47e6b095c280feb))

* inicializo poetry ([`49101fc`](https://gitlab.com/Varasmanu/my-package/-/commit/49101fcb1f95d6a4d57b362f0c7b46dbc245d584))

* Actualizo el pipeline con nuevos jobs ([`adbb8da`](https://gitlab.com/Varasmanu/my-package/-/commit/adbb8da9dbcd77cb740ef72314f3929f455c1976))

* Creo primer pipeline ([`55bf044`](https://gitlab.com/Varasmanu/my-package/-/commit/55bf0444f40eacc39e2cca0cc6a68c77c77eaf25))

* Initial commit ([`0b39708`](https://gitlab.com/Varasmanu/my-package/-/commit/0b39708d468a049656ec099510efba5a9fd111f8))
